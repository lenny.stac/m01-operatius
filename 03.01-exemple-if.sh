#! /bin/bash
#@edt ASIX-M01
#Febrer 2023
#
#
#
#
#	Modificacio del programa 03-exemple-if.sh 
#	per saber l'edat corresponent
#----------------------------------------------------------

if [ $# -ne 1 ]
then
        echo "Error: numero args incorrecte"
        echo "Usage: $0 edat"
        exit 1
fi


edat=$1
if [ $edat -lt 18  ]
then
        echo "edat $edat és menor d'edat"
else 
	echo "edat $edat és major d'edat"	
fi
exit 0
