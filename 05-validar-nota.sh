#! /bin/bash
#@edt ASIX-M01
#Febrer 2023
#
#
#
#	Validacio de la nota si es  < 5 suspes i si 
# 	es >5 o <10 aprobat si la nota es > 10 msg
#	Error.
#----------------------------------------------------------

if [ $# -ne 1 ]
then
	echo "Error:numero args incorrecte"
	echo "Usage: $0 nota"
	exit 1
fi


nota=$1
 
if [ $nota -gt 10 ]
then
	echo "Error:valor incorrecte"
	echo "Usage: $0 nota"
	exit 2
fi

if [ $nota -lt 5 ]
then
	echo "Suspes, $nota"

else
	echo "Aprobat, $nota"
fi

exit 0
