#! /bin/bash
#@edt ASIX-M01
#Març 2023
#
#
#
# Decripcio:   exercicis for,while,case,etc...
#----------------------------------------------------------
#1. Mostrar l’entrada estàndard numerant línia a línia 

num=1
while read -r line
do
	echo "$num: $line"
	((num++))
done
exit 0

#2. Mostar els arguments rebuts línia a línia, tot numerànt-los. 

num=1
while [ -n 1 ]
do
    	echo "$num: $1"
	((num++))
shift
done
exit 0

#2. Mostar els arguments rebuts línia a línia, tot numerànt-los. 

num=1
for line in 
do
	echo "$num: $line"
	((num++))
done
exit 0


#3. Fer un comptador des de zero fins al valor indicat per l’argument rebut. 

MAX=1
num=0
while [ $num -le $MAX ]
do
    	echo "$num"
    	((num++))
done
exit 0

#4. Fer un programa que rep com a arguments números de més (un o més) i indica per a cada mes rebut quants dies té el més. 

for mes in $* 
case $mes in
	1|3|5|7|8|10|12)
		echo “Ente 31 dies”
		;;
	4|6|9|11)
		echo “Ente 30 dies”
		;;
	*)
		echo “Ente 28 dies”
		;;

esac
exit 0

#5. Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.
while read -r line
do
	echo “$line” | cut -c1-50
done
exit 0
#6. Fer un programa que rep com a arguments noms de dies de la setmana i mostra quants dies eren laborables i quants festius. Si l’argument no és un dia de la setmana genera un error per stderr. 
#Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday 
for dia in $*
case $dia in
        "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
                echo "Es un dia laborable"
                ;;
        "disabte"|"diumenge")
                echo "Es un dia festiu"
                ;;
                *)
                        echo “Error: $* ”
                        ;;
esac
exit 0
#7. Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la mostra, si no no.

while read -r line
do
        echo "$line"
done

caracters=$(wc -L $line)
variable=60
if [ $caracters -ge $variable  ]
then
        echo "$line"
fi
exit 0



