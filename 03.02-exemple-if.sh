#! /bin/bash
#@edt ASIX-M01
#Febrer 2023
#
#
#
#
#       Modificacio del programa 03-exemple-if.sh 
#       per saber l'edat corresponent
#----------------------------------------------------------

if [ $# -ne 1 ]
then
        echo "Error: numero args incorrecte"
        echo "Usage: $0 edat"
        exit 1
fi


edat=$1
if [ $edat -lt 18  ]
then
        echo "edat $edat és menor d'edat"
elif [ $edat -lt 65 ] 
then
	echo "edat $edat es de poblacio activa"

else
	echo "edat $edat és jubilat"
fi
exit 0

