#! /bin/bash
#@edt ASIX-M01
#Febrer 2023
#
#
#
# 
#
#
#----------------------------------------------------------
ERR_ARGS=1
ERR_NODIR=2

if [ $# -ne 1 ]
then
	echo "Error:num args incorrecte"
	echo "Usage: $0 /directori"
	exit $ERR_ARGS
fi

directori=$1

if [ ! -d $directori ]
then
	echo "Error: $directori is not directoty"
	echo "Usage: $0 /directory"
	exit $ERR_NODIR
fi

ls -al $directori

exit 0

