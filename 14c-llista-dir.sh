#! /bin/bash
#@edt ASIX-M01
#Març 2023
#
#
#
# Decripcio: programa dir
#----------------------------------------------------------
ERR_ARGS=1
ERR_DIR=2

if [ $# -ne 1 ]
then
        echo "Error:numero args incorrecte"
        echo "Usage: $0 dir"
        exit $ERR_ARGS
fi


 directory= $1
 
if ! [ -d  $directory -o -h $directory -o -e $directory  ]
then
	echo "Error:$directory no és un directory"
	echo "Usage:$0 dir,link,file"
	exit $ERR_DIR
fi


llista=$(ls $dir)
for elem in $llista
do
	if [ -d "$diretory/$llista" ]
	then
		echo "es un directory: $directory"
	elif [ -h "$directory/$llista" ]
	then
		echo "es un link: $directory"
	elif [ -e "$directory/$llista" ]
	then 
		echo "es un regular file: $diretory"

fi

done
exit 0
