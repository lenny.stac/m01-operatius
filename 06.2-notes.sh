#! /bin/bash
#@edt ASIX-M01
#Febrer 2023
#
#
#
#       Validacio de la nota si es  < 5 suspes i si 
#       es >5 o <10 aprobat si la nota es > 10 msg
#       Error.
#
#
#---------------------------------------------------------------
if [ $# -ne 1 ]
then
        echo "Error:numero args incorrecte"
        echo "Usage: $0 nota"
        exit 1
fi


for nota in $*
do
	if ![ $nota -ge 0 -a $nota -le 10 ];then
		echo"Error: $nota no entre [0-10]" >>/dev/stderr
