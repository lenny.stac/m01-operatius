#! /bin/bash
#@edt ASIX-M01
#Març 2023
#
#
#
# 
#
#
#----------------------------------------------------
#8)Lllistar tots els logins numerats


llistat=$(cut -d: -f1  /etc/passwd | sort )
num=1
for arg in $llistat
do
	echo "$num: $arg"
	((num++))
done
exit 0


#7) llista el fitxer del directori actiu numerats

llistat=$(ls)
num=1
for arg in $llistat
do
	echo "$num: $arg"
	((num++))
done
exit 0

#6) numerar els arguments 
num=1


for arg in $*
do 
	echo "$num: $arg"
	num=$((num +1))
done
exit 0


#5) iterar i mostrar la llsita d'arguments

for arg in "$@"
do 
	echo $arg
done
exit 0


#4) iterar i mostrar la llista d'arguments
for arg in $*
do 
	echo $arg
done
exit 0


#3) iterar pel valor d'una variable
llista=$(ls)
for nom in $llista
do 
	echo "$nom"
done
exit 0



#2)iterar per un conjunt d'elemts
for nom in "pere marta pau anna"
do
	echo "$nom"


#1)iterar per un conjunt d'elements

for nom in "pere" "marta" "pau" "anna"
do
	echo "$nom"
done
exit 0
