#! /bin/bash
#@edt ASIX-M01
#Març 2023
#
#
#
# Decripcio: programa dir
#----------------------------------------------------------
ERR_ARGS=1
FI=0

if [ $# -eq 0 ]
then
        echo "Error:numero args incorrecte"
        echo "Usage: $0 dir"
        exit $ERR_ARGS
fi


for directory in $*
do
	if ! [ -d  $directory ]
	then
	echo "Error:$directory no és un directory" 1>&2
else
	llista=$(ls $directory)
	echo "dir:$directory "
	for elem in $llista
	do
		if [ -d "$directory/$elem" ]
		then
			echo -e "\t es un directory: $elem"
		elif [ -h "$directory/$elem" ]
		then
			echo -e "\t es un link: $elem"
		elif [ -e "$directory/$elem" ]
		then 
			echo -e "\t es un regular file: $elem"
		fi
	done
	fi
done
exit $FI
