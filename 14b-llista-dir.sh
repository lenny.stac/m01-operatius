#! /bin/bash
#@edt ASIX-M01
#Març 2023
#
#
#
# Decripcio: programa dir
#----------------------------------------------------------
ERR_ARGS=1

if [ $# -ne 1 ]
then
        echo "Error:numero args incorrecte"
        echo "Usage: $0 dir"
        exit $ERR_ARGS
fi



directory=$1

if ! [ -d  $directory   ]
then
	echo "Error:$directory no és un directory"
	echo "Usage:$0 dir"
fi

num=1

llista=$(ls $directory)
for elem in $llista
do
	echo "$num: $elem"
	((num++))

done
exit 0


