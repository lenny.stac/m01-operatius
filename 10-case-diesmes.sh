#! /bin/bash
#@edt ASIX-M01
#Març 2023
#
#
#
# 
#
#
#---------------------------------------------------------
ERR_ARGS=1
ERR_ARGVL=2
OK=0
if [ $# -ne 1 ]
then 
	echo "Error:num arg incorrecte"
	echo "Usage: $0  mes "
	exit $ERR_ARGS
fi

if [ "$1" = "-h" -o "$1" = "--help" ]
then
  echo "Escola del treball"
  echo "@edt ASIX-M01"
  echo "Usage: $0 mes"
  exit $OK
fi

mes=$1
if ! [ $mes -ge 1 -a $mes -le 12 ]
then
  echo "Error, mes $mes no vàlid"
  echo "Mes pren valors del [1-12]"
  echo "Usage: $0 mes"
  exit $ERR_ARGVL
fi


case "$mes" in
  "2")
    dies=28;;
  "4"|"6"|"9"|"11")
    dies=30;;
  *)
    dies=31;;
esac  
exit 0

										
