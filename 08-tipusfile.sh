#! /bin/bash
#@edt ASIX-M01
#Febrer 2023
#
#
#
# 
#
#
#----------------------------------------------------------
ERR_ARGS=1
ERR_TIPE=2
if [ $# -ne 1 ]
then 
	echo “Error:num arg incorrecte”
	echo ”Usage: $0 /file /directory /link”
	exit ARR_ARGS
fi  
file=$1

if [ ! -f $file ]
then
	echo ”Error:valor no regular file”
elif [ ! -h $file -o ! -e $file ]
then
	echo “Error:valor no link”
else
	echo ”Error: valor no directory ”
	exit ERR_TIPE
fi

if [ -f $file ]
then
	echo ”es un regular file “$file”
elif [ -h $file ]
then
	echo “es un link $file”
else
	echo ”es un directori $file ”
fi

exit 0

